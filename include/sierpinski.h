/* sierpinski.h */

#include <memory>

#ifndef SIERPINSKI_H
#define SIERPINSKI_H

class Sierpinski {
public:
    Sierpinski (const size_t&);
    ~Sierpinski ();
    
    Sierpinski (const Sierpinski&) = delete;
    Sierpinski (const Sierpinski&&) = delete;
    Sierpinski& operator= (const Sierpinski&) = delete;

    void run ();

private:
    class impl;
    std::unique_ptr<impl> pimpl;
};

#endif
