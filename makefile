OUT = bin/sierpinski
CC = g++
IFLAGS = -Iinclude
CFLAGS = -Wall -std=c++17
LDFLAGS = -lGL -lSDL2 -lSDL2_gfx -lpthread 
SOURCES = src/*

OBJECTS = $(SOURCES:.c=.o)

all:
	mkdir bin
	$(CC) $(IFLAGS) $(CFLAGS) -o $(OUT) $(SOURCES) $(LDFLAGS)
clean:
	rm -rf bin
