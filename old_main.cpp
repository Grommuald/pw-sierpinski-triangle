// main.cpp

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>

#include <utility>
#include <thread>
#include <mutex>
#include <vector>
#include <iostream>

using namespace std;

class Utilities {
public:
    static SDL_Texture* get_current_frame (SDL_Window*, SDL_Renderer*);
};

class Sierpinski {
public:
    Sierpinski (const size_t&);
    void run ();

    struct Triangle;
private:
    int get_offset (const int&, const int&); 
    pair<int, int> find_mid (const pair<int, int>&, const pair<int, int>&);
    void execute_sierpinski_part (const Triangle& t, int);
    
    size_t level;

    size_t screen_width { 800 };
    size_t screen_height { 600 };
    
    SDL_Texture* screen_content;
    SDL_Renderer* renderer;
    SDL_Window* window;

    std::mutex render_mutex;

    bool update_screen { true };
};

static bool update_screen = true;

static size_t screen_width;
static size_t screen_height;

static SDL_Texture* screen_content = nullptr;

struct Triangle {
    Triangle (const pair<int, int>& a, const pair<int, int>& b, const pair<int, int>& c) :
        p1 {a}, p2 {b}, p3 {c} {}

    pair<int, int> p1;
    pair<int, int> p2;
    pair<int, int> p3;
};

inline pair<int, int> find_mid (const pair<int, int>& p1, const pair<int, int>& p2) {
    return pair<int, int> { (p1.first + p2.first) / 2, (p1.second + p2.second) / 2 };
}

mutex renderer_mutex;

void execute_sierpinski_part (
        SDL_Renderer* renderer, const Triangle& t, const int& max_level, int current_level) {
    if (current_level > max_level) {
        return;
    }

    pair<int, int> mid_p1p2 = find_mid(t.p1, t.p2);
    pair<int, int> mid_p2p3 = find_mid(t.p2, t.p3);
    pair<int, int> mid_p3p1 = find_mid(t.p3, t.p1);
  
    renderer_mutex.lock();
    trigonRGBA(renderer, 
            mid_p1p2.first, mid_p1p2.second,
            mid_p2p3.first, mid_p2p3.second,
            mid_p3p1.first, mid_p3p1.second,
            255, 0, 0, 255);
    renderer_mutex.unlock();
    
    vector<thread> threads;
    threads.emplace_back(
            thread(execute_sierpinski_part, renderer, Triangle(t.p1, mid_p1p2, mid_p3p1), max_level, current_level+1)); 
    threads.emplace_back(
            thread(execute_sierpinski_part, renderer, Triangle(mid_p1p2, t.p2, mid_p2p3), max_level, current_level+1)); 
    threads.emplace_back(
            thread(execute_sierpinski_part, renderer, Triangle(mid_p2p3, t.p3, mid_p3p1), max_level, current_level+1)); 
    
    for (auto& i : threads) {
        i.join();
    }
}

SDL_Texture* get_current_frame (SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* save_surface = nullptr;
    SDL_Surface* info_surface = nullptr;

    info_surface = SDL_GetWindowSurface(window);

    puts("pixels");
    unsigned char* pixels = 
        new unsigned char [info_surface->w * info_surface->h * info_surface->format->BytesPerPixel];
    
    puts("SDL_RenderReadPixels");
    SDL_RenderReadPixels(
        renderer, 
        &info_surface->clip_rect, 
        info_surface->format->format,
        pixels,
        info_surface->w * info_surface->format->BytesPerPixel
    );
    puts("SDL_CreateRGBSurfaceFrom");
    save_surface = SDL_CreateRGBSurfaceFrom(
        pixels, 
        info_surface->w,
        info_surface->h,
        info_surface->format->BitsPerPixel,
        info_surface->w * info_surface->format->BytesPerPixel,
        info_surface->format->Rmask,
        info_surface->format->Gmask,
        info_surface->format->Bmask,
        info_surface->format->Amask
    );
    puts("SDL_CreateTextureFromSurface");
    SDL_Texture* result_texture = SDL_CreateTextureFromSurface(renderer, save_surface);

    delete[] pixels;
    SDL_FreeSurface(save_surface);
    SDL_FreeSurface(info_surface);

    puts("return result_texture");
    return result_texture;
}

void update_screen_content (
    SDL_Renderer* renderer, SDL_Window* window, const size_t& screen_width, const size_t& screen_height) 
{
    auto get_offset = [] (const int& c, const float& offset) {
        return static_cast<int>(offset * c);
    };

    float shard = 0.05f;
    Triangle main_triangle
    ( 
        { get_offset(screen_width, shard), screen_height - get_offset(screen_height, shard) },
        { get_offset(screen_width, shard), get_offset(screen_width, shard) },
        { screen_width - get_offset(screen_width, shard), screen_height - get_offset(screen_height, shard) }
    );

    filledTrigonRGBA(
            renderer, 
            get<0>(main_triangle.p1), get<1>(main_triangle.p1),
            get<0>(main_triangle.p2), get<1>(main_triangle.p2),
            get<0>(main_triangle.p3), get<1>(main_triangle.p3),
            120, 120, 120, 255);

    execute_sierpinski_part(renderer, main_triangle, 5, 1);
    puts("get_current_frame");
    screen_content = get_current_frame(window, renderer);
}


static int resizing_event_watcher (void* data, SDL_Event* event) {
    if (event->type == SDL_WINDOWEVENT &&
        event->window.event == SDL_WINDOWEVENT_RESIZED) {
        
        SDL_Window* win = SDL_GetWindowFromID(event->window.windowID);
        if (win == (SDL_Window*)data) {
            printf("resizing.....\n");

            screen_width = event->window.data1;
            screen_height = event->window.data2;
            
            update_screen = true;
        }
    }
    return 0;
}

int main (int argc, char** argv) {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        return 1;
    }
    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;
    
    SDL_DisplayMode current_mode;
    if (SDL_GetCurrentDisplayMode(0, &current_mode) != 0) {
        SDL_Log("Could not get display mode: %s", SDL_GetError());
    }
    
    screen_width = current_mode.w;
    screen_height = current_mode.h;

    if (SDL_CreateWindowAndRenderer(screen_width, screen_height, 0, &window, &renderer) != 0) {
        SDL_Quit();
        return 1;
    }
    SDL_AddEventWatch(resizing_event_watcher, window);
    SDL_SetWindowResizable(window, SDL_TRUE);
    
    SDL_bool done = SDL_FALSE;

    while (!done) {
        SDL_Event event; 
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                done = SDL_TRUE;
            }
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
        
        if (update_screen) {
            puts("update_screen_content...");
            update_screen_content(renderer, window, screen_width, screen_height);
            update_screen = false;
        }
        SDL_RenderCopy(renderer, screen_content, nullptr, nullptr);
        SDL_RenderPresent(renderer);
    }

    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }
    SDL_Quit();
    return 0;
}
