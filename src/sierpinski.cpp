/* sierpinski.cpp */

#include "sierpinski.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>

#include <utility>
#include <thread>
#include <mutex>
#include <vector>
#include <iostream>

using namespace std;

class Utils {
public:
    static SDL_Texture* get_current_frame (SDL_Window*, SDL_Renderer*);
};

class Sierpinski::impl {
public:
    impl (const size_t&);
    ~impl ();

    struct Triangle {
        Triangle (const pair<int, int>& a, const pair<int, int>& b, const pair<int, int>& c) :
            p1 {a}, p2 {b}, p3 {c} {}
         
        pair<int, int> p1, p2, p3;
    };
    inline int get_offset (const int& c, const float& offset) {
        return static_cast<int>(offset * c);
    }
    inline pair<int, int> find_mid (const pair<int, int>& p1, const pair<int, int>& p2) {
        return pair<int, int> { (p1.first + p2.first) / 2, (p1.second + p2.second) / 2 };
    }
    
    void execute_sierpinski_part (const Triangle&, size_t);
    void update_screen_content ();
    void update ();

private:
    bool update_screen { true };
                   
    size_t max_level;             
    size_t screen_width { 800 };
    size_t screen_height { 600 };
                                      
    SDL_Texture* screen_content;
    SDL_Renderer* renderer;
    SDL_Window* window;
                                                     
    std::mutex renderer_mutex;

    class CallbackHandler {
    public:
        static void pass_object (Sierpinski::impl*);
        static int resizing_event_watcher (void*, SDL_Event*);
    private:
        static Sierpinski::impl* object;
    };
};

Sierpinski::impl* Sierpinski::impl::CallbackHandler::object { nullptr };

Sierpinski::Sierpinski (const size_t& max_level) {
    pimpl = make_unique<impl>(max_level);
}

Sierpinski::~Sierpinski () {

}

void Sierpinski::run () {
    pimpl->update();
}

SDL_Texture* Utils::get_current_frame (SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* save_surface = nullptr;
    SDL_Surface* info_surface = nullptr;
               
    info_surface = SDL_GetWindowSurface(window);
                    
    puts("pixels");
    unsigned char* pixels = 
        new unsigned char [info_surface->w * info_surface->h * info_surface->format->BytesPerPixel];
                              
    puts("SDL_RenderReadPixels");
    SDL_RenderReadPixels(
        renderer,
        &info_surface->clip_rect,
        info_surface->format->format,
        pixels,
        info_surface->w * info_surface->format->BytesPerPixel
    );
    puts("SDL_CreateRGBSurfaceFrom");
    save_surface = SDL_CreateRGBSurfaceFrom(
                    pixels,
                    info_surface->w,
                    info_surface->h,
                    info_surface->format->BitsPerPixel,
                    info_surface->w * info_surface->format->BytesPerPixel,
                    info_surface->format->Rmask,
                    info_surface->format->Gmask,
                    info_surface->format->Bmask,
                    info_surface->format->Amask
                );
    puts("SDL_CreateTextureFromSurface");
    SDL_Texture* result_texture = SDL_CreateTextureFromSurface(renderer, save_surface);
                                                            
    delete[] pixels;
    SDL_FreeSurface(save_surface);
    SDL_FreeSurface(info_surface);
                                                                           
    puts("return result_texture");
    return result_texture;
}

void Sierpinski::impl::CallbackHandler::pass_object (Sierpinski::impl* obj_ptr) {
    if (obj_ptr) {
        object = obj_ptr;
    }
}

int Sierpinski::impl::CallbackHandler::resizing_event_watcher (void* data, SDL_Event* event) {
    if (event->type == SDL_WINDOWEVENT &&
        event->window.event == SDL_WINDOWEVENT_RESIZED) 
    {
        SDL_Window* win = SDL_GetWindowFromID(event->window.windowID);
        if (win == (SDL_Window*)data) {
            printf("resizing.....\n");
                                            
            object->screen_width = event->window.data1;
            object->screen_height = event->window.data2;
                                                                      
            object->update_screen = true;
        }
    }
    return 0;
}

Sierpinski::impl::impl (const size_t& max_level) : max_level { max_level } {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        exit(1);
    }
    if (!(window = SDL_CreateWindow(
                    "Sierpinski", 
                    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
                    screen_width, screen_height,
                    SDL_WINDOW_RESIZABLE))) {
        SDL_Quit();
        exit(1);
    }
    if (!(renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED))) {
        SDL_Quit();
        exit(1);
    }
    CallbackHandler::pass_object(this);
    SDL_AddEventWatch(CallbackHandler::resizing_event_watcher, window);
}

Sierpinski::impl::~impl () {
    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }
    SDL_Quit();
}

void Sierpinski::impl::execute_sierpinski_part (const Triangle& t, size_t current_level) {
    if (current_level > max_level) {
        return;
    }
 
    pair<int, int> mid_p1p2 = find_mid(t.p1, t.p2);
    pair<int, int> mid_p2p3 = find_mid(t.p2, t.p3);
    pair<int, int> mid_p3p1 = find_mid(t.p3, t.p1);
                
    renderer_mutex.lock();
    trigonRGBA(
            renderer,
            mid_p1p2.first, mid_p1p2.second,
            mid_p2p3.first, mid_p2p3.second,
            mid_p3p1.first, mid_p3p1.second,
            255, 0, 0, 255
    );
    renderer_mutex.unlock();
                               
    vector<thread> threads;
    threads.emplace_back(thread([=] { execute_sierpinski_part(Triangle(t.p1, mid_p1p2, mid_p3p1), current_level+1); } )); 
    threads.emplace_back(thread([=] { execute_sierpinski_part(Triangle(mid_p1p2, t.p2, mid_p2p3), current_level+1); } ));
    threads.emplace_back(thread([=] { execute_sierpinski_part(Triangle(mid_p2p3, t.p3, mid_p3p1), current_level+1); } )); 
                                                   
    for (auto& i : threads) {
        i.join();
    }
}

void Sierpinski::impl::update_screen_content () {
    const float shard = 0.05f;
    Triangle main_triangle
    (
        {   
            get_offset(this->screen_width, shard), 
            this->screen_height - get_offset(this->screen_height, shard) 
        },
        { 
            get_offset(this->screen_width, shard), 
            get_offset(this->screen_width, shard) 
        },
        { 
            this->screen_width - get_offset(this->screen_width, shard), 
            this->screen_height - get_offset(this->screen_height, shard) 
        }
    );
      
    filledTrigonRGBA(
        this->renderer,
        main_triangle.p1.first, main_triangle.p1.second,
        main_triangle.p2.first, main_triangle.p2.second,
        main_triangle.p3.first, main_triangle.p3.second,
        120, 120, 120, 255);
           
    execute_sierpinski_part(main_triangle, 1);
    screen_content = Utils::get_current_frame(window, renderer);
}

void Sierpinski::impl::update () {
    bool done = false;
    
    while (!done) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                done = true;
            }
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
                           
        if (update_screen) {
            puts("update_screen_content...");
            update_screen_content();
            update_screen = false;
        }
        SDL_RenderCopy(renderer, screen_content, nullptr, nullptr);
        SDL_RenderPresent(renderer);
    }
}
