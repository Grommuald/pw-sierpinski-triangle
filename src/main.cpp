/* main.cpp */

#include "sierpinski.h"
#include <string>

int main (int argc, char** argv) {
    if (argc != 2) {
        return 1;
    }
    Sierpinski s (std::stol(std::string(argv[1])));
    s.run();
    
    return 0;
}
